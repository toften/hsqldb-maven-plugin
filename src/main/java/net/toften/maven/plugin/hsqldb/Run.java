package net.toften.maven.plugin.hsqldb;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.hsqldb.Server;

/**
 * @goal run
 */
public class Run
extends
AbstractMojo {
    /**
     * @parameter default-value="xdb"
     */
    private String name;

    /**
     * @parameter default-value=9001
     */
    private int port;

    /**
     * @parameter default-value="localhost"
     */
    private String address;
    
    public void execute() throws MojoExecutionException, MojoFailureException {
        createMemServer(name, address, port).start();
    }
    
    private static Server createMemServer(String name, String address, int port) {
        // set up the rest of properties
        Server server = new Server();
        server.setDatabasePath(0, "mem:" + name);
        server.setDatabaseName(0, name);
        server.setPort(port);
        server.setAddress(address);
        
        server.setLogWriter(new PrintWriter(System.out)); // can use custom writer
        server.setErrWriter(new PrintWriter(System.out)); // can use custom writer

        return server;
    }
    
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        String name = "xdb";
        
        createMemServer(name, "localhost", 9001).start();

        // Login
        Class.forName("org.hsqldb.jdbcDriver" );

        Connection c = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/xdb", "SA", ""); 
        System.out.println(c.getMetaData().getDatabaseProductName());
        ResultSet rs = c.getMetaData().getTableTypes();
        while (rs.next()) {
            System.out.println(rs.getString(1));
        }
        rs.close();
        c.close();

        stop(name);
    }

    private static void stop(String name) throws SQLException {
        Connection c = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/" + name, "SA", "");
        Statement s = c.createStatement();
        s.execute("SHUTDOWN");
        s.close();
        c.close();
    }
}
