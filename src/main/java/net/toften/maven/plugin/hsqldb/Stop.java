package net.toften.maven.plugin.hsqldb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

/**
 * @goal stop
 */
public class Stop
extends
AbstractMojo {

    /**
     * @parameter default-value="xdb"
     */
    private String name;

    /**
     * @parameter default-value=9001
     */
    private int port;

    /**
     * @parameter default-value="localhost"
     */
    private String address;
    
    public void execute() throws MojoExecutionException, MojoFailureException {
        try {
            Class.forName("org.hsqldb.jdbcDriver");
            
            Connection c = DriverManager.getConnection("jdbc:hsqldb:hsql://" + address + ":" + port + "/" + name, "SA", "");
            Statement s = c.createStatement();
            s.execute("SHUTDOWN");
            s.close();
            c.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
