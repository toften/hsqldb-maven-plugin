package net.toften.maven.plugin.hsqldb;

import org.hsqldb.Server;
import org.hsqldb.persist.HsqlProperties;

public class Test {
    public static void main(String[] args) {
        HsqlProperties p = new HsqlProperties();
        p.setProperty("server.database.0","mem:xdb");
        p.setProperty("server.dbname.0","xdb");

        // set up the rest of properties
        Server server = new Server();
        server.setProperties(p);
        server.setLogWriter(null); // can use custom writer
        server.setErrWriter(null); // can use custom writer

        server.start();

    }

}
